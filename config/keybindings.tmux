unbind-key -n C-a
set -g prefix ^A
set -g prefix2 ^A
bind a send-prefix
bind-key -n F10 display-panes \; split-window -h
bind-key -n F11 display-panes \; select-pane -t :.-
bind-key -n F12 display-panes \; select-pane -t :.+
