local ipaddr=$(/bin/ip addr | grep 'eth0\|br0\|bond0\|venet0\|en\w*[os]\|host0' | grep -P -v "inet (127\.0\.|192\.168\.|100\.\d+\.)" | grep "inet " | head -n 1 | awk '{print $2}' | awk -F/ '{print $1}')

local ip='%{$FG[227]%}${ipaddr}%{$reset_color%}'
local time='%{$FG[039]%}%*%{$reset_color%}'
local user='%{$FG[123]%}%n%{$reset_color%}@%{$FG[219]%}%m%{$reset_color%}'
local pwd='%{$FG[195]%}%~%{$reset_color%}'

if [[ $EUID = 0 ]]; then
    local prompt_sign="#"
else
    local prompt_sign="$"
fi

if [[ -e /root/.production ]]; then
    local warning_sign=' %{$FX[blink]$BG[196]%}PRODUCTION%{$reset_color%} '
else
    local warning_sign=''
fi

PROMPT="${ip} ${time} ${user}:${pwd}${warning_sign}${prompt_sign} "
