home = Dir.home
omz = "#{home}/.oh-my-zsh"
etc = "#{home}/.etc"

def mkdir_cp
  lambda do |t, *_|
    mkdir_p File.dirname(t.name)
    cp t.source, t.name
  end
end

task :default => [:omz, :config]
task :config => [:omz_hl, :omz_theme, :htop, :zshrc, :iftoprc, :vimrc, :chsh, :byobu]
task :omz => "#{omz}/oh-my-zsh.sh"
task :omz_hl => "#{omz}/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
task :omz_theme => "#{omz}/custom/themes/msg7086.zsh-theme"
task :htop => "#{home}/.config/htop/htoprc"
task :zshrc => "#{home}/.zshrc"
task :iftoprc => "#{home}/.iftoprc"
task :vimrc => "#{home}/.vimrc"
task :byobu => ["#{home}/.byobu/keybindings.tmux", "#{home}/.byobu/.tmux.conf", "#{home}/.byobu/status"]

file_create "#{omz}/oh-my-zsh.sh" do
  rm_rf omz if File.exist? omz
  sh "git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git #{omz}"
end

file_create "#{omz}/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" => ["#{omz}/oh-my-zsh.sh"] do
  mkdir_p "#{omz}/custom/plugins"
  sh "git clone --depth=1 https://github.com/zsh-users/zsh-syntax-highlighting.git #{omz}/custom/plugins/zsh-syntax-highlighting"
end

file "#{omz}/custom/themes/msg7086.zsh-theme" => ["#{etc}/config/msg7086.zsh-theme", "#{omz}/oh-my-zsh.sh"], &mkdir_cp
file "#{home}/.config/htop/htoprc" => ["#{etc}/config/htoprc"], &mkdir_cp
file "#{home}/.byobu/keybindings.tmux" => ["#{etc}/config/keybindings.tmux"], &mkdir_cp
file "#{home}/.byobu/.tmux.conf" => ["#{etc}/config/.tmux.conf"], &mkdir_cp
file "#{home}/.byobu/status" => ["#{etc}/config/status"], &mkdir_cp
file "#{home}/.vimrc" => ["#{etc}/config/vimrc"], &mkdir_cp

file "#{home}/.zshrc" => ["#{etc}/config/zshrc"] do |t|
  cp t.name, "#{t.name}-#{Time.now.to_i}" if File.exist? t.name
  cp t.source, t.name
end

file "#{home}/.iftoprc" do |t|
  File.write(t.name, 'max-bandwidth: 50M')
end

task :chsh do
  user = File.readlines('/etc/passwd').select{|l| l[ENV['USER']]}.first
  sh 'chsh -s /bin/zsh' if !user['zsh']
end

